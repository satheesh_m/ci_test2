/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.myfirstapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Satheesh_M
 */
public class LoginDAO {

    static Connection conn = null;
    static PreparedStatement ps = null;
    private static final Logger logger = LogManager.getLogger(LoginDAO.class);

    public static void main(String[] argv) throws SQLException {

        try {
            makeJDBCConnection();

            String getQueryStatement = "SELECT * FROM status";

            ps = conn.prepareStatement(getQueryStatement);

            // Execute the Query, and get a java ResultSet
            ResultSet rs = ps.executeQuery();

            // Let's iterate through the java ResultSet
            while (rs.next()) {
                String statuscode = rs.getString("statuscode");
                String description = rs.getString("description");

                // Simply Print the results
                System.out.println("--------------------------------");
                System.out.println(statuscode);
                System.out.println(description);
                System.out.println("--------------------------------");
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally{
            if(ps!=null){
                ps.close();
            }
        }
    }

    private static void makeJDBCConnection() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            log("Congrats - Seems your MySQL JDBC Driver Registered....!");
        } catch (ClassNotFoundException e) {
            log("Sorry, couldn't found JDBC driver. Make sure you have added JDBC Maven Dependency Correctly");
            e.printStackTrace();
            return;
        }

        try {
            // DriverManager: The basic service for managing a set of JDBC drivers.
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/rdbdevdb", "root", "");
            if (conn != null) {
                log("Connection Successful! Enjoy. Now it's time to push data");
            } else {
                log("Failed to make connection!");
            }
        } catch (SQLException e) {
            //log("MySQL Connection Failed!");
            logger.error("MySQL Connection Failed!");
            e.printStackTrace();
            return;
        }

    }

    private static void log(String string) {
        System.out.println(string);
    }
    
    public int add(int x, int y) {
        return x + y;
    }

}
